from django.db import models

class Valorificare(models.Model):

    firma_valorificare = models.CharField(max_length=50)
    tip_deseu = models.CharField(max_length=50)
    cod_deseu = models.CharField(max_length=50)
    cantitate_kg = models.IntegerField()
    cost = models.IntegerField()
    data = models.DateField()

    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.firma_valorificare} {self.tip_deseu}'
