from django.urls import reverse_lazy
from django.views.generic import CreateView


from valorificare.models import Valorificare


class ValorificareCreateView(CreateView):
    template_name = 'valorificare/adauga_valorificare.html'
    model = Valorificare
    form_class =
    success_url = reverse_lazy('home') #ce scop are acest url