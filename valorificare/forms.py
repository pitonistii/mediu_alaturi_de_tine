from django import forms

from valorificare.models import Valorificare


class ValorificareForm(forms.ModelForm):
    class Meta:
        model= Valorificare
        fields = '__all__'
        widgets = {
            
        }

