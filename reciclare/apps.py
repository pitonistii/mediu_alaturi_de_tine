from django.apps import AppConfig


class ReciclareConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'reciclare'
