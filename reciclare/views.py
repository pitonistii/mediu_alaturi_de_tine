from django.urls import reverse_lazy
from django.views.generic import CreateView

from reciclare.forms import ReciclareForm
from reciclare.models import Reciclare


class ReciclareCreateView(CreateView):
    template_name = 'reciclare/adauga_reciclare.html'
    model = Reciclare
    form_class = ReciclareForm
    success_url = reverse_lazy('home')
