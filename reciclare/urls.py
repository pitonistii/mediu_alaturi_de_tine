from django.urls import path
from reciclare import views

urlpatterns = [
    path('adauga-deseu/', views.ReciclareCreateView.as_view(), name='adauga_deseu')
]
